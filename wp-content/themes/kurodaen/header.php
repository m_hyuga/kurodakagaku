<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<!doctype html>
<html lang="ja-JP">
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<!--<![endif]-->
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta charset="UTF-8">
	<meta content="" name="copyright">
	<meta content="黒田化学はプラメカニズムの理念でプラスチックの可能性を追求し、新たな価値を創造します。
「プラメカニズム」とは、世の中のいろいろなニーズとプラスチックを主体とした種々のシーズを結び、新たなメカニズムを生み出すことです。
黒田化学が、長年培ってきたノウハウ・技術力・ネットワークを活かして新たな価値を創造します。" name="description">
	<meta name="viewport" content="user-scalable=yes" />
	<meta name="format-detection" content="telephone=no" />
	<title>	<?php if ( is_search()  ) : ?>
	Search results：<?php $key = wp_specialchars($s, 1); echo $key; ?>｜<?php bloginfo('name'); ?>
	<?php else : ?>
	<?php wp_title( '｜', true, 'right' ); ?>
	<?php endif ?></title>
	<link rel="stylesheet" href="//www.css-reset.com/css/meyer.css" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/common.css" />
	<?php
     $url = $_SERVER["REQUEST_URI"];
		 if(is_home()) : ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/top.css" />
	<?php elseif ( get_post_type() == 'products'  ) : ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/products.css" />
	<?php elseif (strstr($url, 'company')) : ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/company.css" />
	<?php elseif (strstr($url, 'recruit')) : ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/recruit.css" />
	<?php elseif ( is_page('global') ) : ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/global.css" />
	<?php elseif (strstr($url, 'mechanism')) : ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/mechanism.css" />
	<?php elseif ((strstr($url, 'technology')) || get_post_type() == 'project'  || is_singular( 'project' )  ) : ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/technology.css" />
	<?php elseif ( is_search()  ) : ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/search.css" />
	<?php elseif ( is_page('contact') || is_404) : ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/contact.css" />
	<?php endif ?>
	<?php wp_head(); ?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/common/js/common.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/common/js/jquery.smoothScroll.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/common/js/jquery.tile.min.js"></script>
	<?php if(is_home()) : ?>
		<script src="<?php bloginfo('template_url'); ?>/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
		<link href="<?php bloginfo('template_url'); ?>/common/js/jquery.bxslider/jquery.bxslider.css" rel="stylesheet" />
		<script type="text/javascript">
			$(function(){
				$('.bxslider').bxSlider({
					auto: 'true', //オートスライド有
					mode: 'fade', //フェードで切り替え
					autoHover: 'true' //マウスを置いた時オートスライドを止める
				});

				$('.cl_point').hover(function(){ //INDEXボタンのマウスオーバー
					$(this).parent("li").css('backgroundImage', 'url("<?php bloginfo('template_url'); ?>/common/images/top/bg_btn_on.png")');
				}, function(){
					$(this).parent("li").css('backgroundImage', 'url("<?php bloginfo('template_url'); ?>/common/images/top/bg_btn_off.png")');
				});

				$('.cl_point').hover(function(){ //INDEXボタンのマウスオーバー
					$(this).parent("li.bg_02").css('backgroundImage', 'url("<?php bloginfo('template_url'); ?>/common/images/top/bg_btn2_on.png")');
				}, function(){
					$(this).parent("li.bg_02").css('backgroundImage', 'url("<?php bloginfo('template_url'); ?>/common/images/top/bg_btn2_off.png")');
				});

				$('.cl_point').hover(function(){ //INDEXボタンのマウスオーバー
					$(this).parent("li.bg_03").css('backgroundImage', 'url("<?php bloginfo('template_url'); ?>/common/images/top/bg_btn3_on.png")');
				}, function(){
					$(this).parent("li.bg_03").css('backgroundImage', 'url("<?php bloginfo('template_url'); ?>/common/images/top/bg_btn3_off.png")');
				});

			});
		</script>
	<?php elseif (  get_post_type() == 'products' ) : ?>
	<?php elseif (  is_page('integrated') ) : ?>
		<script src="<?php bloginfo('template_url'); ?>/common/js/colorbox-master/jquery.colorbox-min.js"></script>
		<link href="<?php bloginfo('template_url'); ?>/common/js/colorbox-master/colorbox-custom.css" rel="stylesheet" />
		<script type="text/javascript">
			$(document).ready(function(){
				$(".cbox").colorbox({
					inline:true,
					width:'880px'
				});
			});
		</script>
	<?php endif ?>
		<script type="text/javascript">
			$(function(){
				$('a img').hover(function(){
					$(this).attr('src', $(this).attr('src').replace('_off', '_on'));
				}, function(){
					if (!$(this).hasClass('currentPage')) {
						$(this).attr('src', $(this).attr('src').replace('_on', '_off'));
				}
				});
			});
		</script>
		<!--[if lt IE 9]>
			<script type="text/javascript" src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
		<![endif]-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57980601-1', 'auto');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

</script>
	</head>
<body>
<?php if (  is_page('global') ) : ?>
<div id="bg_map">
<div id="wrap">
<?php else : ?>
<div id="wrapper">
<?php endif ?>
	<header id="top">
		<div class="cf">
			<h1><a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/common/images/common/logo_head.png" alt="KURODA KAGAKU CO.LTD"></a></h1>
			<ul id="lnav">
				<li><a href="/">日本語</a></li>
				<li><a href="/en/">English</a></li>
			</ul>
			<dl class="cf">
				<dt><img src="<?php bloginfo('template_url'); ?>/common/images/common/icon_search.png" alt="search"></dt>
				<dd><?php
					// 検索フォームを出力する
					get_search_form();
					// 検索フォームを代入する
					$search_form = get_search_form(false); ?>
				</dd>
			</dl>

		</div>
		<ul class="head_nav cf">
			<li><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav_01_off.png" alt="HOME"></a></li>
			<li><a href="<?php bloginfo('url'); ?>/mechanism/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav_02_off.png" alt="PLA-MECHANISM"></a></li>
			<li><a href="<?php bloginfo('url'); ?>/technology/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav_03_off.png" alt="TECHNOLOGY AND INTEGRATED PRODUCTION SYSTEM"></a></li>
			<li><a href="<?php bloginfo('url'); ?>/global/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav_04_off.png" alt="GLOBAL PRODUCTION SYSTEM"></a></li>
			<li><a href="<?php bloginfo('url'); ?>/products/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav_05_off.png" alt="PRODUCTS"></a></li>
			<li><a href="<?php bloginfo('url'); ?>/company/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav_06_off.png" alt="COMPANY"></a></li>
			<li><a href="<?php bloginfo('url'); ?>/recruit/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav_07_off.png" alt="HUMAN RESOURCE DEVELOPMENT"></a></li>
			<li><a href="<?php bloginfo('url'); ?>/contact/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav_08_off.png" alt="CONTACT US"></a></li>
		</ul>
