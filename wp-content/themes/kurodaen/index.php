<?php
/**
* The main template file
*
* This is the most generic template file in a WordPress theme and one
* of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query,
* e.g., it puts together the home page when no home.php file exists.
*
* @link http://codex.wordpress.org/Template_Hierarchy
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/

get_header(); ?>
<!-- bxslider -->
<ul class="bxslider">
	<li><a href="<?php bloginfo('url'); ?>/mechanism/"><img src="<?php bloginfo('template_url'); ?>/common/images/top/slider_01.jpg" alt="ABOUT PLA-MECHANISM"></a></li>
	<li><a href="<?php bloginfo('url'); ?>/products/"><img src="<?php bloginfo('template_url'); ?>/common/images/top/slider_02.jpg" alt="PRODUCTS"></a></li>
	<li><a href="<?php bloginfo('url'); ?>/technology/integrated/"><img src="<?php bloginfo('template_url'); ?>/common/images/top/slider_04.jpg" alt="INTEGRATED PRODUCTION SYSTEM"></a></li>
	<li><a href="<?php bloginfo('url'); ?>/global/"><img src="<?php bloginfo('template_url'); ?>/common/images/top/slider_05.jpg" alt="GLOBAL PRODUCTION SYSTEM"></a></li>
</ul>
<div class="bg_shadow"></div>
</header>

<div id="wrap" class="cf">

<div id="content">

	<ul class="top_btn cf">

		<li class="mg_l0"><a href="<?php bloginfo('url'); ?>/mechanism/" class="cl_point"></a>
			<h3>About "Pla-Mechanism"</h3>
			<div class="line">
				<p>Kuroda Kagaku creates new value, leveraging our plastics know-how and our technology.</p>
			</div>
			<p class="btn_deta">Click Here for Details</p>
		</li>

		<li><a href="<?php bloginfo('url'); ?>/products/" class="cl_point"></a>
			<h3>Products</h3>
			<div class="line">
				<p>We provide products for a wide variety of fields, from the automotive industry to information devices.</p>
			</div>
			<p class="btn_deta">Click Here for Details</p>
		</li>

		<li><a href="<?php bloginfo('url'); ?>/technology/integrated/" class="cl_point"></a>
			<h3>Integtated Production System</h3>
			<div class="line">
				<p>We have the flexibility to handle all kinds of processes, from development support to molding, assembly, and shipment.</p>
			</div>
			<p class="btn_deta">Click Here for Details</p>
		</li>

		<li class="mg_l0"><a href="<?php bloginfo('url'); ?>/global/" class="cl_point"></a>
			<h3>Global Production System</h3>
			<div class="line">
				<p class="w_255">We have built a global network that allows for production at locations where there is demand.</p>
			</div>
			<p class="btn_deta">Click Here for Details</p>
		</li>

		<li><a href="<?php bloginfo('url'); ?>/recruit/" class="cl_point"></a>
			<h3>Human Resource Development</h3>
			<div class="line">
				<p class="w_255">We actively work to hire and train the individuals whom we expect to play leading roles at Kuroda Kagaku in the future.</p>
			</div>
			<p class="btn_deta">Click Here for Details</p>
		</li>

		<li class="bg_02"><a href="<?php bloginfo('url'); ?>/company/" class="cl_point"></a>
			<h3>Company</h3>
		</li>

		<li class="bg_02 pos_abs"><a href="<?php bloginfo('url'); ?>/contact/" class="cl_point"></a>
			<h3>Contact us</h3>
		</li>


	</ul>

</div><!-- /content -->


<p class="pagetop"><a href="#top"><img src="<?php bloginfo('template_url'); ?>/common/images/common/icon_pagetop.jpg" alt="Top of Page">Top of Page</a></p>
</div><!-- /wrap -->
<?php
get_footer();
