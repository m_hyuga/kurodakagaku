<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<h2><img src="<?php bloginfo('template_url'); ?>/common/images/technology/main_img.jpg" alt="技術力・一貫生産体制"></h2>
</header>

<div id="wrap" class="cf">
<div class="side_nav">
	<img src="<?php bloginfo('template_url'); ?>/common/images/technology/project_side.png" alt="プロジェクト事例一覧">
	<ul>
	<?php query_posts( Array(
						'showposts' => 5,
						'post_type' => 'project'
				));
			if (have_posts()) : while (have_posts()) : the_post(); ?>
				<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
	<?php endwhile; ?>
	<?php endif; ?>
	<?php wp_reset_query(); ?>
	</ul>
</div>

<div id="content" class="ex_deta">
<div class="bg_shadow">
<div class="bg_deta">
<div class="bg_gra">

	<h4><?php the_title(); ?><span><?php the_time('Y-n-j'); ?></span></h4>
	<div class="cf">
		<div class="deta_txt"><?php echo nl2br(post_custom('txt-main')); ?></div>
		<span><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" alt="" width="200" height="150"></span>
	</div>

	<h5>お客様の要望</h5>
	<div class="cf">
		<?php if ( post_custom('img-youbou1') || post_custom('img-youbou2') ) {
		echo '<p class="deta_txt">';} else { echo '<p>';}; ?>
		<?php echo nl2br(post_custom('txt-youbou')); ?></p>
		<span><?php if ( post_custom('img-youbou1') ) {
		$imagefield = get_imagefield('img-youbou1');
		$attachment = get_attachment_object($imagefield['id']);
		echo '<img src="'.$attachment['url'].'" width="200" height="150" />';
		} ?><?php if ( post_custom('img-youbou2') ) {
		$imagefield = get_imagefield('img-youbou2');
		$attachment = get_attachment_object($imagefield['id']);
		echo '<img src="'.$attachment['url'].'" width="200" height="150" />';
		} ?></span>
	</div>
	<h5>黒田化学の課題解決</h5>
	<div class="mg_b10 cf">
			<?php if ( post_custom('img-kadai1') ) {
			echo '<p class="deta_txt">';} else { echo '<p>';}; ?>
			<?php if ( post_custom('ttl-kadai1') ) {
			echo '<span>【'. nl2br(post_custom('ttl-kadai1')) .'】</span>';		} ?>
			<?php echo nl2br(post_custom('txt-kadai1')); ?></p>
		<span><?php if ( post_custom('img-kadai1') ) {
		$imagefield = get_imagefield('img-kadai1');
		$attachment = get_attachment_object($imagefield['id']);
		echo '<img src="'.$attachment['url'].'" width="200" height="150" />';
		} ?></span>
	</div>
	<div class="mg_b10 cf">
			<?php if ( post_custom('img-kadai2') ) {
			echo '<p class="deta_txt">';} else { echo '<p>';}; ?>
			<?php if ( post_custom('ttl-kadai2') ) {
			echo '<span>【'. nl2br(post_custom('ttl-kadai2')) .'】</span>';		} ?>
			<?php if ( post_custom('txt-kadai2') ) {
			echo nl2br(post_custom('txt-kadai2')) ;		} ?></p>
		<span><?php if ( post_custom('img-kadai2') ) {
		$imagefield = get_imagefield('img-kadai2');
		$attachment = get_attachment_object($imagefield['id']);
		echo '<img src="'.$attachment['url'].'" width="200" height="150" />';
		} ?></span>
	</div>
	<div class="mg_b10 cf">
			<?php if ( post_custom('img-kadai3') ) {
			echo '<p class="deta_txt">';} else { echo '<p>';}; ?>
			<?php if ( post_custom('ttl-kadai3') ) {
			echo '<span>【'. nl2br(post_custom('ttl-kadai3')) .'】</span>';		} ?>
			<?php if ( post_custom('txt-kadai3') ) {
			echo nl2br(post_custom('txt-kadai3')) ;		} ?></p>
		<span><?php if ( post_custom('img-kadai3') ) {
		$imagefield = get_imagefield('img-kadai3');
		$attachment = get_attachment_object($imagefield['id']);
		echo '<img src="'.$attachment['url'].'" width="200" height="150" />';
		} ?></span>
	</div>
	<h5>成果と展望・お客様の声</h5>
	<p><?php echo nl2br(post_custom('txt-youbou2')); ?></p>


</div><!-- /bg_gra -->
</div><!-- /bg_deta -->
</div><!-- /bg_shadow -->

</div><!-- /content -->

<p class="pagetop"><a href="#top"><img src="<?php bloginfo('template_url'); ?>/common/images/common/icon_pagetop.jpg" alt="Top of Page">Top of Page</a></p>
</div><!-- /wrap -->
<?php
get_footer();
