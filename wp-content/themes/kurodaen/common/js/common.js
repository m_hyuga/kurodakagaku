$(function(){

	//マウスオーバー切り替え
	$('a img').hover(function(){
		$(this).attr('src', $(this).attr('src').replace('_off', '_on'));
	}, function(){
		if (!$(this).hasClass('currentPage')) {
			$(this).attr('src', $(this).attr('src').replace('_on', '_off'));
		}
	});

	// プラメカニズム高さ調整
	$(window).load(function(){
		$(".bg_real").tile(1); // プラメカニズム
		$(".gl_net li").tile(3); // グローバル生産体制
		$("#car li").tile(); // 製品紹介 自動車関連機器
		$("#car > div, #info > div, #forming > div").tile(); // 製品紹介 自動車関連機器
	});

});

// jQuery(function($) {
//   $('a[href^="#"]').SmoothScroll({
//     duration : 1000,    // スピード（ミリ秒）
//     easing : 'easeOutQuint' // 動き（イージング）
//   });
// });