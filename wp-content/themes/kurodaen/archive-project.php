<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Fourteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<h2><img src="<?php bloginfo('template_url'); ?>/common/images/technology/main_img.jpg" alt="技術力・一貫生産体制"></h2>
</header>

<div id="wrap" class="cf">
<div class="side_nav">
	<img src="<?php bloginfo('template_url'); ?>/common/images/technology/technology_side.png" alt="技術力・一貫生産体制">
	<ul>
				<li><a href="<?php bloginfo('url'); ?>/technology/">技術力</a></li>
				<li><a href="<?php bloginfo('url'); ?>/technology/integrated/">一貫生産体制</a></li>
				<li><a href="<?php bloginfo('url'); ?>/project/">プロジェクト事例紹介</a></li>
	</ul>
</div>


<div id="content" class="ex">

<h3>プロジェクト事例紹介</h3>
<p>黒田化学がサポートした開発プロジェクトの最新事例を一部ご紹介しています。</p>

<ul class="ex_list">
<?php if(have_posts()): while(have_posts()): the_post();?>
	<li>
		<?php if($project_cnt == 0): ?>
		<span class="new"><img src="<?php bloginfo('template_url'); ?>/common/images/technology/icon_new.png" alt="NEW"></span>
		<?php endif; ?>
			<h4><span><?php the_time('Y-n-j'); ?></span><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
			<div class="cf">
				<p><?php $str_maintxt = post_custom('txt-main');
					$count_maintxt = 152;
					$more_maintxt = '...';
					
					echo wp_html_excerpt( $str_maintxt, $count_maintxt, $more_maintxt );
					?></p>
				<span class="pro_img"><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" alt="" width="200" height="150"></span>
			</div>
			<p class="a_deta"><a href="<?php the_permalink(); ?>">詳しくはこちら<img src="<?php bloginfo('template_url'); ?>/common/images/common/r_arrow.jpg" alt="矢印"></a></p>
	</li>
	<?php $project_cnt ++; ?>
<?php endwhile; ?>
<?php endif; ?>
			</ul>

</div><!-- /content -->

<p class="pagetop"><a href="#top"><img src="<?php bloginfo('template_url'); ?>/common/images/common/icon_pagetop.jpg" alt="Top of Page">Top of Page</a></p>
</div><!-- /wrap -->
<?php
get_footer();
