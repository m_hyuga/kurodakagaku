<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Fourteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div id="blog_content" class="cf">
<?php get_sidebar(); ?>
<div id="main" class="cf">
<section class="bloglist_content">
<?php if ( is_child_theme()) : ?>
	<?php if ( is_category('12') ) : ?>
	<h3><img src="<?php bloginfo('template_url'); ?>/common/images/blog/ttl_listheader_event.jpg" alt="event"></h3>
	<?php elseif ( is_category('5') ) : ?>
	<h3><img src="<?php bloginfo('template_url'); ?>/common/images/blog/ttl_listheader_grume.jpg" alt="小路のちょっといい店"></h3>
	<?php elseif ( is_category('10') or is_category('4') ) : ?>
	<h3><img src="<?php bloginfo('template_url'); ?>/common/images/blog/ttl_listheader_kankyo.jpg" alt="知っ得！自転車環境"></h3>
	<ul>
		<li class="left"><a href="<?php site_top_url(); ?>/machiblog/category/10/"><img src="<?php bloginfo('template_url'); ?>/common/images/blog/btn_kankyo.jpg" alt="自転車利用環境の記事はこちら" class="over"></a></li>
		<li class="right"><a href="<?php site_top_url(); ?>/machiblog/category/4/"><img src="<?php bloginfo('template_url'); ?>/common/images/blog/btn_cycle.jpg" alt="コミュニティサイクルの記事はこちら" class="over"></a></li>
	</ul>
	<br class="clear">
<?php endif ?>
<?php elseif ( is_post_type_archive('osusumeroot') ) : ?>
<h3><img src="<?php bloginfo('template_url'); ?>/common/images/blog/ttl_listheader_root.jpg" alt="おすすめルート"></h3>
<?php endif ?>

<br><?php if ( is_category() ) { // カテゴリの有無のチェック
					 echo '「';
					 echo single_cat_title().'」 '; } ?>記事の一覧ページです
<br>
<br>
<ul class="bloglist_topics cf">
<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
	<li>
		<dl class="cf">
			<dt><a href="<?php the_permalink() ?>" class="over "style="background-image: url('<?php if ( has_post_thumbnail() ) { // アイキャッチの有無のチェック
		echo wp_get_attachment_url( get_post_thumbnail_id() );
		}else{
		echo bloginfo("template_url");
		echo '/common/images/common/noimage.jpg';} ?>')"></a></dt>
			<dd>
			<time><?php the_time('Y.m.d'); ?></time>
			<h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
			<p><?php
		$theContentForPreSingle = mb_substr(strip_tags($post-> post_content), 0, 150);
		$theContentForPreSingle = preg_replace('/\［caption(.+?)\/caption\］/','',$theContentForPreSingle);
		$theContentForPreSingle = preg_replace('/\［gallery(.+?)\］/','',$theContentForPreSingle);
		echo $theContentForPreSingle.'...';
		?></p>
			</dd>
		</dl>
	</li>
	<?php endwhile; ?>
<?php else : ?>
	<br class="clear">
    記事が見つかりません
<?php endif; ?>
</ul>
	<div id="page" class="cf">
			<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
			<div class="clear"></div>
	</div>
<?php if ( is_child_theme() &&  is_category('12') ) : ?>
<div class="event_list cf">
<h3><img src="<?php bloginfo('template_url'); ?>/common/images/blog/ttl_event.png" alt="EVENT"></h3>
<ul>
	<li><a href="http://www.machi-nori.jp/machinorixbonbori/"><img src="<?php bloginfo('template_url'); ?>/common/images/blog/btn_bonbori.png" alt="ぼんぼり祭り×まちのり" class="over"></a></li>
	<li><a href="http://www.machi-nori.jp/machiblog/10922/"><img src="<?php bloginfo('template_url'); ?>/common/images/blog/btn_komachi.png" alt="こまちなみなーと" class="over"></a></li>
	<li><a href="http://www.machi-nori.jp/machiblog/10879/"><img src="<?php bloginfo('template_url'); ?>/common/images/blog/btn_seseragienmusubi.png" alt="せせらぎ縁結びまつり" class="over"></a></li>
</ul>
</div>
<?php endif ?>
</section>
</div><!-- main end -->
</div><!-- content end -->


<?php
get_footer();
