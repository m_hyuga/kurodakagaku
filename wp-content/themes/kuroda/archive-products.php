<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Fourteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<h2><img src="<?php bloginfo('template_url'); ?>/common/images/products/main_img.jpg" alt="PRODUCTS 製品紹介"></h2>
</header>

<div id="wrap" class="cf">
<div class="side_nav">
	<img src="<?php bloginfo('template_url'); ?>/common/images/products/products_side.png" alt="製品紹介PRODCUTS">
	<ul>
		<li><a href="#car">自動車関連機器</a></li>
		<li><a href="#info">情報機器</a></li>
		<li><a href="#forming">特殊成形製品</a></li>
	</ul>
</div>


<div id="content">
<?php wp_reset_query(); ?>
<div class="bg_shadow">
	<div id="car" class="bg_gradation cf">
	<h3>自動車関連機器</h3>
	<?php query_posts( Array(
						'showposts' => -1,
						'order' => 'asc',
						'post_type' => 'products', //カスタム投稿名
						'taxonomy' => 'products_cat',
						'term' => 'car'
				));
			if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div><img src="<?php if ( has_post_thumbnail() ) { // アイキャッチの有無のチェック
			echo wp_get_attachment_url( get_post_thumbnail_id() );}; ?>" alt="<?php the_title(); ?>"><p><?php the_title(); ?></p></div>
	<?php endwhile; ?>
			<?php else : ?>
	<?php endif; ?>
<?php wp_reset_query(); ?>
	</div><!-- /car_comp -->
</div><!-- /bg_shadow -->
<p class="pagetop"><a href="#top"><img src="<?php bloginfo('template_url'); ?>/common/images/common/icon_pagetop.jpg" alt="上へ">ページの先頭へ戻る</a></p>

<div class="bg_shadow">
	<div id="info" class="bg_gradation cf">
	<h3>情報機器</h3>
	<?php query_posts( Array(
						'showposts' => -1,
						'order' => 'asc',
						'post_type' => 'products', //カスタム投稿名
						'taxonomy' => 'products_cat',
						'term' => 'info'
				));
			if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div><img src="<?php if ( has_post_thumbnail() ) { // アイキャッチの有無のチェック
			echo wp_get_attachment_url( get_post_thumbnail_id() );}; ?>" alt="<?php the_title(); ?>"><p><?php the_title(); ?></p></div>
	<?php endwhile; ?>
			<?php else : ?>
	<?php endif; ?>
<?php wp_reset_query(); ?>
	</div><!-- /car_comp -->
</div><!-- /bg_shadow -->
<p class="pagetop"><a href="#top"><img src="<?php bloginfo('template_url'); ?>/common/images/common/icon_pagetop.jpg" alt="上へ">ページの先頭へ戻る</a></p>

<div class="bg_shadow">
	<div id="forming" class="bg_gradation cf">
	<h3>特殊成形製品</h3>
	<?php query_posts( Array(
						'showposts' => -1,
						'order' => 'asc',
						'post_type' => 'products', //カスタム投稿名
						'taxonomy' => 'products_cat',
						'term' => 'forming'
				));
			if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div><img src="<?php if ( has_post_thumbnail() ) { // アイキャッチの有無のチェック
			echo wp_get_attachment_url( get_post_thumbnail_id() );}; ?>" alt="<?php the_title(); ?>"><p><?php the_title(); ?></p></div>
	<?php endwhile; ?>
			<?php else : ?>
	<?php endif; ?>
<?php wp_reset_query(); ?>
	</div><!-- /car_comp -->
</div><!-- /bg_shadow -->


</div><!-- /content -->

<p class="pagetop"><a href="#top"><img src="<?php bloginfo('template_url'); ?>/common/images/common/icon_pagetop.jpg" alt="上へ">ページの先頭へ戻る</a></p>
</div><!-- /wrap -->
<?php
get_footer();
