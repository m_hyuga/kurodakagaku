<?php
/**
* The main template file
*
* This is the most generic template file in a WordPress theme and one
* of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query,
* e.g., it puts together the home page when no home.php file exists.
*
* @link http://codex.wordpress.org/Template_Hierarchy
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/

get_header(); ?>
<!-- bxslider -->
<ul class="bxslider">
	<li><a href="<?php bloginfo('url'); ?>/mechanism/"><img src="<?php bloginfo('template_url'); ?>/common/images/top/slider_01.jpg" alt="プラメカニズムについて"></a></li>
	<li><a href="<?php bloginfo('url'); ?>/products/"><img src="<?php bloginfo('template_url'); ?>/common/images/top/slider_02.jpg" alt="商品紹介"></a></li>
	<li><a href="<?php bloginfo('url'); ?>/technology/integrated/"><img src="<?php bloginfo('template_url'); ?>/common/images/top/slider_04.jpg" alt="一貫生産体制"></a></li>
	<li><a href="<?php bloginfo('url'); ?>/global/"><img src="<?php bloginfo('template_url'); ?>/common/images/top/slider_05.jpg" alt="グローバル生産体制"></a></li>
</ul>
<div class="bg_shadow"></div>
</header>

<div id="wrap" class="cf">

<div id="content">

	<ul class="top_btn cf">

		<li class="mg_l0"><a href="<?php bloginfo('url'); ?>/mechanism/" class="cl_point"></a>
			<h3>プラメカニズムについて<span><img src="<?php bloginfo('template_url'); ?>/common/images/top/eng_01.png" alt="ABOUT PLA-MECHANISM"></span></h3>
			<p>プラスチックのノウハウや技術力を活かして、新たな価値を創造します。</p>
			<p class="btn_deta">詳しくはこちら</p>
		</li>

		<li><a href="<?php bloginfo('url'); ?>/products/" class="cl_point"></a>
			<h3>製品紹介<span><img src="<?php bloginfo('template_url'); ?>/common/images/top/eng_02.png" alt="PRODUCTS"></span></h3>
			<p>自動車関連分野、情報機器分野など、幅広い分野へ製品を供給しています。</p>
			<p class="btn_deta">詳しくはこちら</p>
		</li>

		<li><a href="<?php bloginfo('url'); ?>/technology/integrated/" class="cl_point"></a>
			<h3>一貫生産体制<span><img src="<?php bloginfo('template_url'); ?>/common/images/top/eng_04.png" alt="INTEGTATED PRODUCTION SYSTEM"></span></h3>
			<p>開発協力から成形、組立、出荷まであらゆるプロセスに柔軟に対応します。</p>
			<p class="btn_deta">詳しくはこちら</p>
		</li>

		<li class="mg_l0"><a href="<?php bloginfo('url'); ?>/global/" class="cl_point"></a>
			<h3>グローバル生産体制<span><img src="<?php bloginfo('template_url'); ?>/common/images/top/eng_05.png" alt="GLOBAL PRODUCTION SYSTEM"></span></h3>
			<p class="w_255">需要のあるところで生産するグローバルネットワーク体制を構築しています。</p>
			<p class="btn_deta">詳しくはこちら</p>
		</li>

		<li><a href="<?php bloginfo('url'); ?>/recruit/" class="cl_point"></a>
			<h3>採用情報・人材育成<span><img src="<?php bloginfo('template_url'); ?>/common/images/top/eng_06.png" alt="RECRUIT & HUMAN RESOURCE DEVELOPMENT"></span></h3>
			<p class="w_255">黒田化学のこれからを担う人材の採用・育成を積極的に行っています。</p>
			<p class="btn_deta">詳しくはこちら</p>
		</li>

		<li class="bg_02"><a href="<?php bloginfo('url'); ?>/company/" class="cl_point"></a>
			<h3>企業情報<span><img src="<?php bloginfo('template_url'); ?>/common/images/top/eng_07.png" alt="COMPANY"></span></h3>
		</li>

		<li class="bg_02 pos_abs"><a href="<?php bloginfo('url'); ?>/contact/" class="cl_point"></a>
			<h3>お問い合わせ<span><img src="<?php bloginfo('template_url'); ?>/common/images/top/eng_08.png" alt="CONTACT US"></span></h3>
		</li>


	</ul>

</div><!-- /content -->


<p class="pagetop"><a href="#top"><img src="<?php bloginfo('template_url'); ?>/common/images/common/icon_pagetop.jpg" alt="上へ">ページの先頭へ戻る</a></p>
</div><!-- /wrap -->
<?php
get_footer();
