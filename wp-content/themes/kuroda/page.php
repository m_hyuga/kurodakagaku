<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<?php the_content(); ?>
<?php endwhile; endif; ?>
<p class="pagetop"><a href="#top"><img src="<?php bloginfo('template_url'); ?>/common/images/common/icon_pagetop.jpg" alt="上へ">ページの先頭へ戻る</a></p>
</div><!-- /wrap -->
<?php
get_footer();
