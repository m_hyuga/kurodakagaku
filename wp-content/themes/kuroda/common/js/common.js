$(function(){

	//マウスオーバー切り替え
	$('a img').hover(function(){
		$(this).attr('src', $(this).attr('src').replace('_off', '_on'));
	}, function(){
		if (!$(this).hasClass('currentPage')) {
			$(this).attr('src', $(this).attr('src').replace('_on', '_off'));
	}
	});

});

// jQuery(function($) {
//   $('a[href^="#"]').SmoothScroll({
//     duration : 1000,    // スピード（ミリ秒）
//     easing : 'easeOutQuint' // 動き（イージング）
//   });
// });