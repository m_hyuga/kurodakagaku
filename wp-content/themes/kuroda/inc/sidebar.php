<?php
/**
 * The Sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<div id="side_left">
<?php if ( is_child_theme() || !is_home() ) : ?>
<?php elseif ( is_home() ) : ?>
<div class="side_weather">
<?php
$request = "https://www.kimonolabs.com/api/b9f1cg3i?apikey=ziIlcCCJqRZAELtwJvw26nyJD2jXhWtj";//URLの中身は上記jsonファイルと同一
$response = file_get_contents($request);
$results = json_decode($response, TRUE);

//---今日の天気
$now_date = $results['results']['collection1'][0]['now_dates']['text'];
$now_icon = $results['results']['collection1'][1]['now_dates']['src'];
$now_wether = $results['results']['collection1'][1]['now_dates']['alt'];

//---週間天気
$next_date = $results['results']['collection1'][5]['now_dates']['text'];
$next_icon = $results['results']['collection1'][6]['now_dates']['src'];
$next_icon_min = str_replace('law', 'saw', $next_icon);
$next_wether = $results['results']['collection1'][6]['now_dates']['alt'];

$week_date = $results['results']['collection2'][0];
$week_wether = $results['results']['collection2'][1];
$week_icon = $results['results']['collection3'];
$cnt = 1;


echo '今日の天気　'.$now_date.'<br><img src="'.$now_icon.'" />'.$now_wether.'<br>';
echo '<p class="weather_tab">一週間の天気</p>';
echo $next_date.'　<img src="'.$next_icon_min.'" widht="35" height="21" />'.$next_wether.'<br>';
echo $week_date['week01'].'　<img src="'.$week_icon[0]['week_icon']['src'].'" widht="35" height="21" />'.$week_icon[0]['week_icon']['alt'].'<br>';
echo $week_date['week02'].'　<img src="'.$week_icon[1]['week_icon']['src'].'" widht="35" height="21" />'.$week_icon[1]['week_icon']['alt'].'<br>';
echo $week_date['week03'].'　<img src="'.$week_icon[2]['week_icon']['src'].'" widht="35" height="21" />'.$week_icon[2]['week_icon']['alt'].'<br>';
echo $week_date['week04'].'　<img src="'.$week_icon[3]['week_icon']['src'].'" widht="35" height="21" />'.$week_icon[3]['week_icon']['alt'].'<br>';
echo $week_date['week05'].'　<img src="'.$week_icon[4]['week_icon']['src'].'" widht="35" height="21" />'.$week_icon[4]['week_icon']['alt'].'<br>';
echo $week_date['week06'].'　<img src="'.$week_icon[5]['week_icon']['src'].'" widht="35" height="21" />'.$week_icon[5]['week_icon']['alt'].'<br>';
?>
<ul id="weather"></ul>
</div>
<?php endif ?>
<div class="side_banner01">
	<ul>
		<li><a href=""><img src="<?php bloginfo('template_url'); ?>/common/images/common/side/bnr_port.jpg" alt="金沢へ電車でお越しの方へ。" class="over" /></a></li>
	</ul>
</div>

<div class="side_search">
	<a href="<?php site_top_url(); ?>/office/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/side/img_toiawase.jpg" alt="まちのり事務局へのお問い合わせ" class="over"></a>
	<?php // 検索フォームを出力する
			get_search_form();
			// 検索フォームを代入する
			$search_form = get_search_form(false); ?>
</div>


<div class="side_topics">
	<h2><img src="<?php bloginfo('template_url'); ?>/common/images/blog/tt_accesstop.png" alt="アクセス記事 TOP5" /></h2>
<?php switch_to_blog(2);

echo	get_template_part( 'template-sidetopix' );

restore_current_blog();
wp_reset_query(); ?>


</div>
<?php if ( is_child_theme() ) : ?>
<div class="side_ad">
	<ul>
		<li><a href="" target="_blank"><img src="<?php bloginfo('template_url'); ?>/common/images/blog/left_ad.jpg" alt="ad" class="over" /></a></li>
		<li><a href="" target="_blank"><img src="<?php bloginfo('template_url'); ?>/common/images/blog/left_ad.jpg" alt="ad" class="over" /></a></li>
	</ul>
</div>
<?php else : ?>
<div class="side_banner02">
	<ul>
		<li><a href="<?php site_top_url(); ?>/machiblog/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/side/btn_stuffblog.jpg" alt="まちのり STUFF BLOG" class="over" /></a></li>
	</ul>
</div>
<?php endif ?>
</div><!-- side_left end -->
