<?php
/*
Template Name: bgcolor-Template
*/
get_header(); ?>
<style>
#wrap {	width: auto;}
</style>
<div id="contents">
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<?php the_content(); ?>
<?php endwhile; endif; ?>
</div>
<!-- content end -->
<?php
get_footer();
