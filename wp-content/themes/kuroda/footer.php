<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<footer>
	<p><small>Copyright &copy; 2014 Kuroda Kagaku Co.Ltd. All Rights Reserved.</small></p>
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>