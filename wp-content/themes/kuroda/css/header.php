<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/common/css/reset.css" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/common/css/common.css" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/common/css/page.css" />
	<?php wp_deregister_script('jquery'); ?>
	<?php if ( !is_page('machi-nori-original') ) : ?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
<link href="<?php bloginfo('template_url'); ?>/common/js/jquery.bxslider/jquery.bxslider.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/js/colorbox/colorbox.css" />
<script src="<?php bloginfo('template_url'); ?>/common/js/colorbox/jquery.colorbox-min.js"></script>
	<?php else : ?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/jquery.easing.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/ui.core.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/jquery.scrollfollow.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/jquery.cookie.js"></script>
	<?php endif ?>
<script>
$(document).ready(function(){
	<?php if ( is_child_theme() || !is_home() ) : ?>
	$(".main_topics li:nth-child(4n)").css("margin-right", "0px");
	<?php elseif ( is_home() ) : ?>
  $('ul.slider').bxSlider({
        controls:false,  
        auto: true,  
        speed: 1200,  
        pause: 3000,  
				autoHover:true 
		});
	$(".main_topics li:nth-child(3n)").css("margin-right", "0px");
	$(".main_topics li:nth-child(3n+1)").css("clear", "left");
	<?php endif ?>
	<?php if ( is_page('machi-nori-original') ) : ?>
		$( '#ugoku' ).scrollFollow(
		{
			speed: 1000,
			offset: 0,
			container: 'page-wrap',
			killSwitch: 'killSwitch'
		}
	);
	<?php endif ?>

	
});
</script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/common.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/rollover2.js"></script>
<link rel="alternate" type="application/rss+xml" title="Simple Colors &raquo; フィード" href="http://machi-nori.skr.jp/feed/" /><!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
	<?php wp_head(); ?>

</head>
<body id="pagetop">
<div id="wrap">
	<header class=" cf">
		<h1><a href="<?php site_top_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav/nav_home.png" alt="まちのり" /></a></h1>
		<p class="head_01"><img src="<?php bloginfo('template_url'); ?>/common/images/common/img_head_txt.png" alt="金沢公共レンタサイクル" /></p>
		<p class="head_02"><img src="<?php bloginfo('template_url'); ?>/common/images/common/img_head_19port.png" alt="金沢市内19ポート" /></p>
		<ul>
			<li><img src="<?php bloginfo('template_url'); ?>/common/images/common/img_head_machinori.png" alt="新幹線を降りたら、まちのりでどうぞ。" /></li>
			<li><img src="<?php bloginfo('template_url'); ?>/common/images/common/img_head_shinkansen.jpg" alt="2015年春・北陸新幹線開業" /></li>
		</ul>
		<nav>
			<ul>
				<li><a href="<?php site_top_url(); ?>/usage/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav/nav_riyo_off.png" alt="利用方法" /></a></li>
				<li><a href="<?php site_top_url(); ?>/price/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav/nav_ryokin_off.png" alt="料金" /></a></li>
				<li><a href="<?php site_top_url(); ?>/port/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav/nav_port_off.png" alt="ポート" /></a></li>
				<li><a href="<?php site_top_url(); ?>/wicket/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav/nav_madoguchi_off.png" alt="提携窓口" /></a></li>
				<li><a href="<?php site_top_url(); ?>/faq/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav/nav_faq_off.png" alt="FAQ" /></a></li>
			</ul>
		</nav>
	</header><!-- header end -->

