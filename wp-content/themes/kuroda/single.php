<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
	<div id="blog_content" class="cf">
<?php get_sidebar(); ?>

<?php if(have_posts()): while(have_posts()): the_post(); ?>

<div id="main" class="cf">
<section class="blogpage_content">
<?php if( get_post_type() === 'osusumeroot' ){
		if ( has_post_thumbnail() ) { // アイキャッチの有無のチェック
		echo '<p class="page_image">';
		the_post_thumbnail('full');
		echo '</p>';
}}

if ( post_custom('txt-komidashi') || post_custom('txt-omidashi') ) { // 大・小見出しの有無のチェック
	echo '<h3>';
	if ( post_custom('txt-komidashi') ) { // 小見出しの有無のチェック
		 echo '<span>';
		 echo nl2br(post_custom('txt-komidashi'));
		 echo '</span>';
	}
	if ( post_custom('txt-omidashi') ) { // 大見出しの有無のチェック
		 echo nl2br(post_custom('txt-omidashi'));
	}
	echo '</h3>';
}

if ( post_custom('txt-midashi') ) { // 見出し下テキストの有無のチェック
	 echo '<p class="txt_datail">';
	 echo nl2br(post_custom('txt-midashi'));
	 echo '</p>';
}


 ?>
	<h4><?php the_title(); ?></h4>
	<div class="btn_iine">
	<?php if(function_exists("wp_social_bookmarking_light_output_e")){wp_social_bookmarking_light_output_e();}?>
	</div>
	<ul class="btn_catedate">
		<li><?php if ( has_category() ) { // カテゴリの有無のチェック
					 echo 'カテゴリー：' .get_the_category_list( '、' ); } ?></li>
		<li><?php the_time('Y.m.d'); ?></li>
	</ul>
	<br class="clear">
	<div class="blogpage_main">
	<?php the_content(); ?>
	</div>
	</section>

		<?php if ( is_child_theme() ) : ?>
<?php $author_know = get_the_author();
		if ( $author_know != 'admin' && $author_know != '' ) {
        echo 'by ';
				echo get_the_author();
    }
?><br><br>
		<?php related_posts(); ?>
		<?php endif ?>

<?php endwhile; ?>
<?php endif; ?>


	
</div><!-- main end -->
</div><!-- content end -->

<?php
get_footer();
