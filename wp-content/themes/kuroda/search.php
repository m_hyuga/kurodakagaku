<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<h2><img src="<?php bloginfo('template_url'); ?>/common/images/search/main_img.jpg" alt="検索結果"></h2>
</header>

<div id="wrap" class="cf">


<div id="content">

<div class="bg_box">
	<div class="bg_gra">
	<?php $allsearch =& new WP_Query("s=$s&posts_per_page=-1");
	$key = wp_specialchars($s, 1);
	$count = $allsearch->post_count;
	if($count!=0){
	// 検索結果を表示:該当記事あり
			echo '<p class="seach_keywords">"'.$key.'"の検索結果<span class="find">'.$count.'件見つかりました。</span></p>';
	}
	else {
	// 検索結果を表示:該当記事なし
			echo '<p class="seach_keywords">"'.$key.'"の検索結果<span class="find">関連する記事は見つかりませんでした</span></p>';
	}
	?>
	
	<?php if(have_posts()): while(have_posts()): the_post();?>
		<div class="find_con">
			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><span class="p_date">掲載日／<?php the_time('Y-n-j'); ?></span></h3>
			<p><?php echo mb_substr(strip_tags($post-> post_content), 0, 210); ?></p>
			<p class="a_deta"><a href="<?php the_permalink(); ?>"><img src="<?php bloginfo('template_url'); ?>/common/images/common/r_arrow.jpg" alt="矢印">​詳しくはこちら</a></p>
		</div><!-- /find_con -->
	<?php endwhile; ?>
	<?php endif; ?>
	</div>
</div>

</div><!-- /content -->
<div id="page" class="cf">
<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
<div class="clear"></div>

</div><!-- /wrap -->

<p class="pagetop"><a href="#top"><img src="<?php bloginfo('template_url'); ?>/common/images/common/icon_pagetop.jpg" alt="上へ">ページの先頭へ戻る</a></p>
</div><!-- /wrap -->




<?php
get_footer();
