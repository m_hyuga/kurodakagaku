<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<h2><img src="<?php bloginfo('template_url'); ?>/common/images/search/main_img.jpg" alt="検索結果"></h2>
</header>

<div id="wrap" class="cf">


<div id="content">
<h3>お探しのページは見つかりませんでした。</h3>
<dl class="cf">
<dt>申し訳ありません、お探しのページは見つかりませんでした。<br><br><a href="<?php bloginfo('url'); ?>">トップページへ戻る</a></dt>
</dl>

</div><!-- /content -->


<p class="pagetop"><a href="#top"><img src="<?php bloginfo('template_url'); ?>/common/images/common/icon_pagetop.jpg" alt="上へ">ページの先頭へ戻る</a></p>
</div><!-- /wrap -->
<?php
get_footer();
