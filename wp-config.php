<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、MySQL、テーブル接頭辞、秘密鍵、言語、ABSPATH の設定を含みます。
 * より詳しい情報は {@link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86 
 * wp-config.php の編集} を参照してください。MySQL の設定情報はホスティング先より入手できます。
 *
 * このファイルはインストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さず、このファイルを "wp-config.php" という名前でコピーして直接編集し値を
 * 入力してもかまいません。
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'demdm_kuroda');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'kuroda');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'Mafia001');

/** MySQL のホスト名 */
define('DB_HOST', 'localhost');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'A_G 7On?HW>4oWM;O]2{i4CNx9]!iHC_JBHFz+@JbN8mLVxQMi4(STg6l(tnU#=|');
define('SECURE_AUTH_KEY',  'qM-h+;7O=>]=~!T2-FXYzj|0TLpF]RZGIUQs k:D`|k-Kf#8yjj4%*HnXk]MxA_C');
define('LOGGED_IN_KEY',    'Juf/HU3)pvK+Wr-xtBH(1=uLZ0EF|5dYffScy0[oI2b2L{ 05N1*10P9`q`o1a+@');
define('NONCE_KEY',        'e((|:vx~LdVOZ01My=t!]$8xcDkT0SQ+Zjfs`+Av-7a|sY4UH2I%B|9<V6S`__$b');
define('AUTH_SALT',        ']*h6ACz-QF2ZCbpCTOPAoQ&RJy`VPU.(|.`fW&4Y>vT:%*3$+k{1a2<(gr_nq@C^');
define('SECURE_AUTH_SALT', 'iHC$}-~[x%ui0*Fw8!cQ{pN3_2&`fW&O41#HN+K5vD 3?3h;<}K0<z)[Uwom][tK');
define('LOGGED_IN_SALT',   '0&0F;{&L3Du5j:~T49E|nTI/) 9gDQSyM(0x8-)A*EuXc7~vP9irl<|& E~AQesx');
define('NONCE_SALT',       'i|J&`UDEZ|lE(!u=6zd)-UpZy^+$Hd#}QjnE76 QUmrYj]+~s=-s_D#t+vuAN)NL');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 */
define('WP_DEBUG', false);



define('WP_ALLOW_MULTISITE', true);

define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'demdm.net');
define('PATH_CURRENT_SITE', '/kuroda/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);
/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
